Title:
An FFT-based framework for predicting corrosion-driven damage in fractal porous media

Authors:
Mohit Pundir, David S. Kammer, Ueli Angst

Date:
2023/01/20

Description:
The repository contains the scripts and jupter notebooks to reproduce the results and the figures as used in the paper.

- File **notebooks/Effect_differential_operator.ipynb** reproduces the comparison between different differential operators (Figure 2, Section 2.5).
- File **notebooks/Corrosion_driven_fracture.ipynb**  simulates the various corrosion-driven mechanisms within a Fractal pore space  (Figure 3, Section 3.1).
- File **scripts/multi_scale_2d.py**  reproduces the results for multi-scale simulation for different types  of RVEs (Section 3.2, Section 4).
- File **notebooks/multiscale_results_analysis.ipynb**  reads the data produced from script `scripts/multi_scale_2d.py` and reproduces the comparison between different RVEs (Figure 5, 6, 7).

