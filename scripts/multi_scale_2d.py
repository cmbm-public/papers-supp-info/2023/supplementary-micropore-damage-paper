#!/usr/bin/env python
# coding: utf-8

from manager import Manager

from utils import *

import numpy as np
import scipy.sparse.linalg as sp
import itertools
import porespy as ps

import argparse


# ------------------------------------------- ##
# Input arguments
# ------------------------------------------- ##
parser = argparse.ArgumentParser()
parser.add_argument('--type',              type=str,   default='fractal')
parser.add_argument('--rve',               type=int,   default=1)
parser.add_argument('--flux',              type=float, default=1e-8)
parser.add_argument('--nb_steps',          type=int,   default=1000)
parser.add_argument('--time',              type=int,   default=2000)
parser.add_argument('--idx',               type=str,   default='fractal-1')
parser.add_argument('--path',              type=str,   default='./')
parser.add_argument('--schema',            type=str,   default='output') 
parser.add_argument('--file',              type=open,  action=LoadFromFile)
args = parser.parse_args()


manager = Manager(args.path, args.schema,
                  uid=args.idx)
manager.initialize(parameters=vars(args))
                     
database_location = manager.getDatabaseLocation()


length = 1 # mm
ndim = 2


if args.type == 'fractal':
    fractal = np.loadtxt('../inputs/fractal-higher.txt')
    delta_space = length/fractal.shape[0]
    structure = fractal.T

elif args.type == 'euclidean':
    euclidean = np.loadtxt('../inputs/eucledian-same-network-higher.txt')
    delta_space = length/euclidean.shape[0]
    structure = euclidean.T

    
isolated_pores = ps.filters.find_disconnected_voxels(structure)


def compute_Ghat_2_1(structure, length=1, operator='forward-difference'):
    ndim = len(structure.shape)
    N     = ndim*(structure.shape[0],)    # number of voxels (assumed equal for all directions)
    
    # PROJECTION IN FOURIER SPACE #############################################
    Ghat2_1 = np.zeros((ndim,ndim)+ N, dtype='complex') # zero initialize
    freq = [np.arange(-(N[ii]-1)/2.,+(N[ii]+1)/2.) for ii in range(ndim)]
    for i,j in itertools.product(range(ndim),repeat=2):
        for ind in itertools.product(*[range(n) for n in N]):
            q = np.empty(ndim, dtype='complex')
            Dop = np.empty(ndim, dtype='complex')
            for ii in range(ndim):
                q[ii] = 2*np.pi*freq[ii][ind[ii]] ## frequency vector
                if operator == 'fourier':
                    Dop[ii] = 1j*q[ii]                                   
                elif operator == 'central-difference':
                    Dop[ii] = 1j*np.sin(q[ii]*delta_space)/delta_space   
                elif operator == '4-order-cd':
                    Dop[ii] = -1j*(8*np.sin(q[ii]*delta_space)/(6*delta_space) - np.sin(2*q[ii]*delta_space)/(6*delta_space))   
                elif operator == '8-order-cd':
                    Dop[ii] = -1j*(8*np.sin(q[ii]*delta_space)/(5*delta_space) - 2*np.sin(2*q[ii]*delta_space)/(5*delta_space) + 8*np.sin(3*q[ii]*delta_space)/(105*delta_space) - np.sin(4*q[ii]*delta_space)/(140*delta_space) ) 
                elif operator == 'forward-difference':
                    Dop[ii] = (np.exp(1j*q[ii]*delta_space)-1)/delta_space
                else:
                    raise RuntimeError('operator incorrectly defined')
        
            if not Dop.dot(np.conjugate(Dop)) == 0:          # zero freq. -> mean
                Dop_inverse = np.conjugate(Dop)/(Dop.dot(np.conjugate(Dop)))
                Ghat2_1[i,j][ind] = Dop[i]*Dop_inverse[j]      

    return Ghat2_1


def perform_diffusion(structure, A, macro_gradient, Ghat, solver=sp.lgmres):
    ndim  = 2                             # number of dimensions (works for 2D and 3D)
    N     = ndim*(structure.shape[0],)    # number of voxels (assumed equal for all directions)

    # auxiliary values
    prodN = np.prod(np.array(N)) # number of grid points
    ndof  = ndim*prodN # number of degrees-of-freedom
    vec_shape=(ndim,)+N # shape of the vector for storing DOFs

    
    E = np.zeros(vec_shape); 
    E[0] = macro_gradient[0] # set macroscopic loading
    E[1] = macro_gradient[1] # set macroscopic loading
     
    # OPERATORS ###############################################################
    dot21  = lambda A,v: np.einsum('ij...,j...  ->i...',A,v)
    fft    = lambda V: np.fft.fftshift(np.fft.fftn (np.fft.ifftshift(V),N))
    ifft   = lambda V: np.fft.fftshift(np.fft.ifftn(np.fft.ifftshift(V),N))
    G_fun  = lambda V: np.real(ifft(dot21(Ghat,fft(V)))).reshape(-1)
    A_fun  = lambda v: -dot21(A,v.reshape(vec_shape))
    DA_fun = lambda v: G_fun(A_fun(v))

    # NEWTON-CONJUGATE GRADIENT SOLVER ###############################################
    aux      =            E.copy()
    En       = np.linalg.norm(aux)
    iiter    = 0

    bd       = -DA_fun(E) # initial residual
    
    nb_iterations = 0
    def callback(x):
        nonlocal nb_iterations
        nb_iterations += 1
    
    while True:
        e, _=  solver(
                    A=sp.LinearOperator(shape=(ndof, ndof), matvec=DA_fun, dtype='float'), 
                    b=bd, callback=callback,
                    tol=1e-8, atol=1e-8)
        aux += e.reshape(vec_shape)
        bd   = -DA_fun(aux) 
    
        if np.abs(np.linalg.norm(e))/En<1e-10 and iiter>0:
            print('Diffusion Calculation converged :', nb_iterations)
            break # check convergence
        iiter += 1

    aux = aux.reshape(-1)
        
    return aux.reshape(vec_shape), A_fun(aux).reshape(vec_shape), nb_iterations



def compute_periodic_fluctuations(structure, concentration_gradients, ndim=2):
    N = structure.shape[0]
    shape  = structure.shape

    # (inverse) Fourier transform (for each tensor component in each direction)
    fft  = lambda x: np.fft.fftshift(np.fft.fftn (np.fft.ifftshift(x),[N,N]))
    ifft = lambda x: np.fft.fftshift(np.fft.ifftn(np.fft.ifftshift(x),[N,N]))
    dot11  = lambda A1,B1: np.einsum('ixy   ,ixy   ->xy    ',A1,B1)


    x_2     = np.zeros([ndim   ,N,N],dtype='int64')  # position vectors
    q_2     = np.zeros([ndim   ,N,N],dtype='float')  # frequency vectors

    # - set "x_2" as position vector of all grid-points
    x_2[0],x_2[1] = np.mgrid[:N,:N]

    # - convert positions "x_2" for frequencies "q_2"
    for i in range(ndim):
        freq   = np.arange(-(shape[i]-1)/2,+(shape[i]+1)/2,dtype='int64')
        q_2[i] = 2*np.pi*freq[x_2[i]]

    tau     = concentration_gradients.reshape([ndim, N, N]).copy()
    R       = 1j*(np.multiply(q_2[0], fft(tau[0, :, :])) + np.multiply(q_2[1],fft(tau[1, :, :])))
    q_2     = q_2.astype(float)
    Q       = dot11(q_2,q_2)
    Z       = Q==0
    Q[Z]    = 1.
    norm    = 1./Q
    norm[Z] = 0.
    R  *= norm
    phi = np.real(ifft(-R))
    
    return phi



def compute_micro_concentration(structure, A, concentration_gradients, macro_concentration=1, length=1, ndim=2):
    phi = compute_periodic_fluctuations(structure=structure, concentration_gradients=concentration_gradients)
    
    N  = ndim*(structure.shape[0],)    # number of voxels (assumed equal for all directions)

    vec_shape=(ndim,)+N # shape of the vector for storing DOFs
    

    dot11  = lambda A1,B1: np.einsum('ixy   ,ixy   ->xy    ',A1,B1)
    dot21  = lambda A,v: np.einsum('ij...,j...  ->i...',A,v)
    A_fun  = lambda v: -dot21(A,v.reshape(vec_shape))
    
    x = np.linspace(0, length, num=structure.shape[0])
    xx = np.zeros(vec_shape)
    xx[0], xx[1] = np.meshgrid(x, x)
    E_x = dot11(concentration_gradients, xx) 

    avg_flux = np.zeros(len(structure.shape))
    avg_flux_pores = np.zeros(len(structure.shape))
    avg_flux_solid = np.zeros(len(structure.shape))

    for i in range(len(structure.shape)):
        avg_flux[i] = -np.mean(-A_fun(concentration_gradients.reshape(-1)).reshape(vec_shape)[i, :, :])
        avg_flux_pores[i] = np.mean(np.ma.array(A_fun(concentration_gradients.reshape(-1)).reshape(vec_shape)[i, :, :], mask=1-structure))
        avg_flux_solid[i] = np.mean(np.ma.array(A_fun(concentration_gradients.reshape(-1)).reshape(vec_shape)[i, :, :], mask=structure))

    xsi = np.zeros_like(E_x)
    xsi[structure==1] = np.linalg.norm(avg_flux_pores)*macro_concentration/np.linalg.norm(avg_flux) - np.mean(np.ma.array(E_x, mask=1-structure))
    xsi[structure==0] = np.linalg.norm(avg_flux_solid)*macro_concentration/np.linalg.norm(avg_flux) - np.mean(np.ma.array(E_x, mask=structure))

    concentration =  E_x + phi + xsi
    return concentration, phi



def compute_eigen_strain(structure, concentration, delta_space, density=3.4e-3, 
                         threshold_concentration=1e-3/1e6, ndim=2, molar_mass=89):
    # computing precipiate concentration
    precipitation = np.where(concentration > threshold_concentration, concentration-threshold_concentration, 0)
    
    cell_volume = delta_space**ndim
    moles_precipitation = precipitation*cell_volume
    volume_precipitation = moles_precipitation*molar_mass/density

    eigen_strain = (volume_precipitation-cell_volume)/(cell_volume)
    eigen_strain = np.where(eigen_strain > 0, eigen_strain, 0)*structure
    
    precipitate_fraction = np.abs(volume_precipitation)/(cell_volume)
    precipitate_fraction = np.where(precipitate_fraction > 0.9, 0.9, precipitate_fraction)
    precipitate_fraction = np.where(structure == 0, 0, precipitate_fraction)
    porosity_reduction = np.where(precipitate_fraction > 0, 1-precipitate_fraction, 1 )
    porosity_reduction = np.where(structure == 0, 1, porosity_reduction)

    return eigen_strain, precipitation, porosity_reduction



def compute_Ghat_4_2(structure, length=1, operator='forward-difference'):
    ndim   = len(structure.shape)
    NN     = ndim*(structure.shape[0],)    # number of voxels (assumed equal for all directions)
    
    Ghat4_2 = np.zeros((ndim,ndim, ndim, ndim)+ NN, dtype='complex') # zero initialize
    freq = [np.arange(-(NN[ii]-1)/2.,+(NN[ii]+1)/2.) for ii in range(ndim)]
    delta   = lambda i,j: float(i==j)               # Dirac delta function

    for i,j,l,m in itertools.product(range(ndim),repeat=4):
        for ind in itertools.product(*[range(n) for n in NN]):
            q = np.empty(ndim, dtype='complex')
            Dop = np.empty(ndim, dtype='complex')
            for ii in range(ndim):
                q[ii] = 2*np.pi*freq[ii][ind[ii]]   ## frequency vector
                if operator == 'fourier':
                    Dop[ii] = 1j*q[ii]                  ## [fourier operator]
                elif operator == 'forward-difference':
                    Dop[ii] = (np.exp(1j*q[ii]*delta_space)-1)/delta_space             
                elif operator == 'central-difference':
                    Dop[ii] = 1j*np.sin(q[ii]*delta_space)/delta_space    
                elif operator == '4-order-cd':
                    Dop[ii] = -1j*(8*np.sin(q[ii]*delta_space)/(6*delta_space) - np.sin(2*q[ii]*delta_space)/(6*delta_space))    ## [fourth-order central difference operator]
                elif operator == '8-order-cd':
                    Dop[ii] = -1j*(8*np.sin(q[ii]*delta_space)/(5*delta_space) - 2*np.sin(2*q[ii]*delta_space)/(5*delta_space) + 8*np.sin(3*q[ii]*delta_space)/(105*delta_space) - np.sin(4*q[ii]*delta_space)/(140*delta_space) ) 
              
            
            if not Dop.dot(np.conjugate(Dop)) == 0:          # zero freq. -> mean
                Dop_inverse = np.conjugate(Dop)/(Dop.dot(np.conjugate(Dop)))
                Ghat4_2[i,j,l,m][ind] = delta(i,m)*Dop[j]*Dop_inverse[l]

    return Ghat4_2



def compute_stress(structure, K, mu, eigen_strain, Ghat4_2, macro_strain=[0, 0]):
    N = structure.shape[0]
    ndof   = ndim**2*N**2 # number of degrees-of-freedom
    shape  = [N,N]         # number of voxels in all directions
    
    # tensor operations/products: np.einsum enables index notation, avoiding loops
    # e.g. ddot42 performs $C_ij = A_ijkl B_lk$ for the entire grid
    trans2 = lambda A2   : np.einsum('ijxy         ->jixy  ',A2   )
    trace2 = lambda A2   : np.einsum('iixy         ->xy    ',A2   )
    ddot22 = lambda A2,B2: np.einsum('ijxy  ,jixy  ->xy    ',A2,B2)
    ddot42 = lambda A4,B2: np.einsum('ijklxy,lkxy  ->ijxy  ',A4,B2)
    ddot44 = lambda A4,B4: np.einsum('ijklxy,lkmnxy->ijmnxy',A4,B4)
    dot11  = lambda A1,B1: np.einsum('ixy   ,ixy   ->xy    ',A1,B1)
    dot22  = lambda A2,B2: np.einsum('ijxy  ,jkxy  ->ikxy  ',A2,B2)
    dot24  = lambda A2,B4: np.einsum('ijxy  ,jkmnxy->ikmnxy',A2,B4)
    dot42  = lambda A4,B2: np.einsum('ijklxy,lmxy  ->ijkmxy',A4,B2)
    dyad22 = lambda A2,B2: np.einsum('ijxy  ,klxy  ->ijklxy',A2,B2)

    # identity tensor (single tensor)
    i    = np.eye(3)
    # identity tensors (grid)
    I2    = np.einsum('ij,xy'          ,                  np.eye(2)   ,np.ones([N,N]))
    I    = np.einsum('ij,xy'          ,                  i   ,np.ones([N,N]))
    I4   = np.einsum('ijkl,xy->ijklxy',np.einsum('il,jk',i,i),np.ones([N,N]))
    I4rt = np.einsum('ijkl,xy->ijklxy',np.einsum('ik,jl',i,i),np.ones([N,N]))
    I4s  = (I4+I4rt)/2.
    II   = dyad22(I,I)
    I4d    = (I4s-II/3.)

    # (inverse) Fourier transform (for each tensor component in each direction)
    fft  = lambda x: np.fft.fftshift(np.fft.fftn (np.fft.ifftshift(x),[N,N]))
    ifft = lambda x: np.fft.fftshift(np.fft.ifftn(np.fft.ifftshift(x),[N,N]))

    # functions for the projection 'G', and the product 'G : K : eps'
    G        = lambda A2   : np.real( ifft( ddot42(Ghat4_2,fft(A2)) ) ).reshape(-1)
    K_deps   = lambda depsm: ddot42(K4,depsm.reshape(ndim,ndim,N,N))
    G_K_deps = lambda depsm: G(K_deps(depsm))

    # stiffness tensor                [grid of tensors]
    # initial tangent operator: the elastic tangent
    K4     = (K*II+2.*mu*I4d)[:2,:2,:2,:2]
    
    #set eigen strain
    eigen_strains       = np.zeros([ndim,ndim,N,N])
    eigen_strains[0, 0] = eigen_strain
    eigen_strains[1, 1] = eigen_strain
    
    # ----------------------------- NEWTON ITERATIONS -----------------------------
    # initialize stress and strain tensor                         [grid of tensors]
    sig      = np.zeros([ndim,ndim,N,N])
    eps      = np.zeros([ndim,ndim,N,N])

    # set macroscopic loading
    DE       = np.zeros([ndim,ndim,N,N])

    DE[0, 0] = macro_strain[0]
    DE[1, 1] = macro_strain[1]

    # initial residual: distribute "DE" over grid using "K4"
    b        = -G_K_deps(DE)
    eps     +=           DE  #+ eigen_strains
    En       = np.linalg.norm(eps)
    iiter    = 0
    
    nb_iterations = 0
    def callback(x):
        nonlocal nb_iterations
        nb_iterations += 1

    # iterate as long as the iterative update does not vanish
    while True:
        depsm,_ = sp.lgmres(
            tol=1e-8, atol=1e-8,
            A = sp.LinearOperator(shape=(ndof,ndof),matvec=G_K_deps,dtype='float'),
            b = b, callback=callback
        )                                     # solve linear system using CG
        eps += depsm.reshape(ndim,ndim,N,N)   # update DOFs (array -> tens.grid)
        sig  = ddot42(K4,eps-eigen_strains)                 # new residual stress
        b     = -G(sig)                       # convert residual stress to residual
   
        if np.abs(np.linalg.norm(depsm))<1e-10 and iiter>0:
            print('Stress Calculations converged : ', nb_iterations)
            break # check convergence
        iiter += 1
        
    return eps, sig


# material parameters + function to convert to grid of scalars
param  = lambda X, M0,M1: M0*np.ones_like(X)*(X)+M1*np.ones_like(X)*(1-X)


def compute_strain_energies(sig, eps, K, mu, structure):
    N = structure.shape[0]
  
    ddot22 = lambda A2,B2: np.einsum('ijxy  ,jixy  ->xy    ',A2,B2)
    trace2 = lambda A2   : np.einsum('iixy         ->xy    ',A2   )
    I2    = np.einsum('ij,xy'          ,                  np.eye(2)   ,np.ones([N,N]))

  
    strain_energy = 0.5*ddot22(sig, eps)
    strain_energy *= (1-structure) 

    # decomposition of strain and strain energy
    macluay_plus = lambda A : 0.5*(A+np.abs(A))
    macluay_minus = lambda A : 0.5*(A-np.abs(A))

    eps_dev = eps - trace2(eps)*I2/3             # deviatoric strain
    esp_plus = macluay_plus(trace2(eps))
    esp_minus = macluay_minus(trace2(eps))

    strain_energy_plus = 0.5*np.multiply(K, esp_plus**2) + np.multiply(mu, ddot22(eps_dev, eps_dev))
    strain_energy_minus = 0.5*np.multiply(K, esp_minus**2)

    # remove energies in the pore phase
    strain_energy_plus *= (1-structure)
    strain_energy_minus *= (1-structure)

    # condition to have 0 dmaage where positive strain energy < negative straina energy
    strain_energy_plus = np.where(strain_energy_plus < strain_energy_minus, 0, strain_energy_plus)

    return strain_energy, strain_energy_plus, strain_energy_minus



def compute_fracture(structure, history_field, Gc, initial_guess, l0=1e-2, k=1e-3, solver=sp.lgmres):
    N = structure.shape[0]
    ndim = len(structure.shape)
    shape  = [N,N]         # number of voxels in all directions
   
    dot = lambda A,B : np.einsum('ij,ji->ij', A, B)
    dot11  = lambda A1,B1: np.einsum('ixy   ,ixy   ->xy    ',A1,B1)
    
     # (inverse) Fourier transform (for each tensor component in each direction)
    fft  = lambda x: np.fft.fftshift(np.fft.fftn (np.fft.ifftshift(x),[N,N]))
    ifft = lambda x: np.fft.fftshift(np.fft.ifftn(np.fft.ifftshift(x),[N,N]))

    
    x_2      = np.zeros([ndim   ,N,N],dtype='int64')  # position vectors
    q_2      = np.zeros([ndim   ,N,N],dtype='int64')  # frequency vectors
    Dq_2     = np.zeros([ndim   ,N,N],dtype='complex')  # frequency vectors

    # - set "x_2" as position vector of all grid-points
    x_2[0],x_2[1] = np.mgrid[:N,:N]

    # - convert positions "x_2" for frequencies "q_2"
    for i in range(ndim):
        freq   = np.arange(-(shape[i]-1)/2,+(shape[i]+1)/2,dtype='int64')
        q_2[i] = 2*np.pi*freq[x_2[i]]
        Dq_2[i] = 1j*q_2[i]
        
    # computing laplacian operator
    Q       = dot11(Dq_2,Dq_2)
    
    penalty = 0
    laplacian = lambda y : np.real(ifft( Q*fft(y) ))
    phasefield = lambda d : dot(history_field + 0.5*Gc/l0 + penalty, d) - 0.5*l0*dot(Gc, laplacian(d)) 
    GA_fun = lambda y : phasefield(y.reshape(shape)).reshape(-1)

    # initial residual
    bp = history_field - phasefield(initial_guess) 
    bp = bp.reshape(-1)
    
    nb_iterations = 0
    def callback(x):
        nonlocal nb_iterations
        nb_iterations += 1

    # solving phasefield equation 
    d, info =solver(
          A=sp.LinearOperator(shape=(N*N, N*N), matvec=GA_fun, dtype='float'),
            b=bp, 
            tol=1e-8, atol=1e-8, callback=callback
        )
    d = d.reshape(shape)
    d += initial_guess
    
    print('PhaseField Calculations converged :', nb_iterations)
    return d.T




# ## Multiscale


def compute_porosity_reduction(structure, precipitation, density):
    cell_volume = (length/structure.shape[0])**2
    moles_precipitation = precipitation*density
    volume_precipitation = moles_precipitation*cell_volume

    precipitate_fraction = np.abs(volume_precipitation)/(cell_volume)
    precipitate_fraction = np.where(precipitate_fraction > 0.9, 0.9, precipitate_fraction)
    precipitate_fraction = np.where(structure == 0, 0, precipitate_fraction)
    porosity_reduction = np.where(precipitate_fraction > 0, 1-precipitate_fraction, 1 )
    porosity_reduction = np.where(structure == 0, 1, porosity_reduction)
    
    return porosity_reduction


# Diffusion parameters
diffusion_coefficients = { 'pore' : 1., 'solid' :  1e-6} # mm2/s
A = np.einsum('ij,...->ij...',np.eye(ndim), 
                (structure-isolated_pores)*diffusion_coefficients['pore']
              + (isolated_pores)*diffusion_coefficients['solid']
              + (1-structure)*diffusion_coefficients['solid']) # material coefficients 
Ghat2_1 = compute_Ghat_2_1(structure-isolated_pores, operator='forward-difference')

# Elasticity parameters
elastic_modulus = {'solid' :  10e3,   'pores' : 1e3} #N/mm2
poisson_modulus = {'solid' :  0.2, 'pores' : 0.45}

bulk_modulus = {}
bulk_modulus['solid'] = elastic_modulus['solid']/(3*(1-2*poisson_modulus['solid']))
bulk_modulus['pores'] = elastic_modulus['pores']/(3*(1-2*poisson_modulus['pores']))

shear_modulus = {}
shear_modulus['solid'] = elastic_modulus['solid']/(2*(1+2*poisson_modulus['solid']))
shear_modulus['pores'] = elastic_modulus['pores']/(2*(1+2*poisson_modulus['pores']))

K0      = param(structure, bulk_modulus['pores'], bulk_modulus['solid'])               # bulk  modulus                   [grid of scalars]
mu0     = param(structure, shear_modulus['pores'], shear_modulus['solid'])   

Ghat4_2 = compute_Ghat_4_2(structure=structure, operator='forward-difference', length=length)

# Damage paramters
l0 = 0.01 # actual value is 0.05 for considered parameters
gc = 1e-2 # 10 J/m2 
k  = 1e-3

N     = ndim*(structure.shape[0],)    # number of voxels (assumed equal for all directions)
vec_shape=(ndim,)+N # shape of the vector for storing DOFs


Nx = 100
Nt = args.nb_steps
T = args.time

bar = np.linspace(0, Nx*10*length, Nx+1)    # mesh points in space
dx  = bar[1] - bar[0]
t   = np.linspace(0, T, Nt+1)    # mesh points in time
dt  = t[1] - t[0]
u   = np.zeros(Nx+1)           # unknown u at new time level
u_1 = np.zeros(Nx+1)           # u at the previous time level

# Set initial condition u(x,0) = I(x)
for i in range(0, Nx+1):
    u_1[i] = 0.
    
flux = args.flux #  mol/mm2/s
u_1[0] = flux*dt

damage = np.zeros_like(structure)
history_field = np.zeros_like(structure)

nb_grid_points = structure.shape[0]*structure.shape[1]

outputs={'gradient.macro'                  : float(0),
         'damage.macro'                    : float(0),
         'precipitate.macro'               : float(0),
         'sigma_x.macro'                   : float(0),
         'sigma_y.macro'                   : float(0),
         'sigma_xy.macro'                   : float(0),
         'porosity_increase.macro'         : float(0),
         'porosity_decrease.macro'         : float(0),
         'diff_coefficient.macro'          : float(0),
         'concentration.macro'             : np.zeros((1, len(u_1))),
         'timings'                         : float(0),
         'damage.micro'                    : np.zeros((1, nb_grid_points)),
         'structure'                       : np.zeros((1, nb_grid_points))
         }

manager.registerQuantities(outputs)



for n, time in enumerate(t):
    print('time :', time)
    
    # Compute u at inner mesh points
    gradu_1 = (u_1[1] - u_1[0])/dx
    gradu_2 = (u_1[2] - u_1[1])/dx
    gradu_3 = (u_1[3] - u_1[2])/dx

    aux, fluxes, nb_iterations = perform_diffusion(structure=structure-isolated_pores, 
                                                   macro_gradient=[np.abs(gradu_1), 0], 
                                                   A=A, 
                                                   Ghat=Ghat2_1, solver=sp.bicgstab)

    avg_properties = np.zeros(len(structure.shape))
    for i in range(len(structure.shape)):
        avg_properties[i] = np.mean(-fluxes[i, :, :])/np.abs(gradu_1)
   
    a = avg_properties[0]
       
    F = a*dt/dx**2

    for i in range(1, Nx):
        u[i] = u_1[i] + F*(u_1[i-1] - 2*u_1[i] + u_1[i+1])

    # Insert boundary conditions
    u[0] = flux*dt*n
    u[Nx] = 0

    # Update u_1 before next step
    u_1[:]= u

    # microscale stress and damages
    if n < 10:
        continue
    
    if args.rve == 1:
        conc = 0.5*(u_1[1] + u_1[0])
    elif args.rve == 2:
        conc = 0.5*(u_1[2] + u_1[1])
        aux, fluxes, nb_iterations = perform_diffusion(structure=structure-isolated_pores, 
                                                       macro_gradient=[np.abs(gradu_2), 0], 
                                                       A=A, 
                                                       Ghat=Ghat2_1, solver=sp.bicgstab)
    elif args.rve == 3:
        conc = 0.5*(u_1[3] + u_1[2])
        aux, fluxes, nb_iterations = perform_diffusion(structure=structure-isolated_pores, 
                                                       macro_gradient=[np.abs(gradu_3), 0], 
                                                       A=A, 
                                                       Ghat=Ghat2_1, solver=sp.bicgstab)
    
        
    concentration, phi = compute_micro_concentration(structure=structure-isolated_pores,
                                                     A=A,
                                                     concentration_gradients=aux, 
                                                     macro_concentration=conc)
    eigen_strain, precipitation, porosity_reduction = compute_eigen_strain(structure=structure, 
                                                       concentration=concentration, 
                                                       delta_space=delta_space)

    mu = ((1-damage)**2 + k*(1-structure))*mu0
    K  = ((1-damage)**2 + k*(1-structure))*K0
    
    eps, sig = compute_stress(structure=structure, 
                              K=K, mu=mu,
                              eigen_strain=eigen_strain,
                              Ghat4_2=Ghat4_2)
    
    strain_energy, strain_energy_plus, strain_energy_minus = compute_strain_energies(sig=sig, 
                                                                                     eps=eps,
                                                                                     K=K, mu=mu, 
                                                                                     structure=structure)
    
    history_field = np.maximum(strain_energy_plus, history_field)

    damage = compute_fracture(structure=structure, 
                              history_field=history_field, 
                              initial_guess=damage,
                              Gc=param(structure, gc, gc), l0=l0, k=k, 
                              solver=sp.lgmres)

    outputs['diff_coefficient.macro'] = a
    outputs['timings'] = time
    outputs['gradient.macro'] =  np.abs(gradu_1) 
    outputs['sigma_x.macro']  =  np.mean(sig[0, 0, :, :])
    outputs['sigma_xy.macro']  =  np.mean(sig[0, 1, :, :])
    outputs['sigma_y.macro']  =  np.mean(sig[1, 1, :, :]) 
    outputs['precipitate.macro'] =  np.mean(precipitation) 
    outputs['damage.macro'] =  np.mean(damage) 
    outputs['porosity_decrease.macro'] = np.mean(structure*porosity_reduction)
    outputs['porosity_increase.macro'] = np.mean(structure+damage)
    outputs['concentration.macro'] = u_1.reshape((1, len(u_1)))
    outputs['structure'] = structure.reshape(-1).reshape((1, nb_grid_points))
    outputs['damage.micro'] = damage.reshape(-1).reshape((1, nb_grid_points))
    
    manager.pushQuantity(outputs)

manager.finalize()


