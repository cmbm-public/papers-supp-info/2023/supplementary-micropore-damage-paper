import subprocess
import uuid
import itertools
import h5py
import os
import subprocess
from manager import Runner


def create_sbatch_script(file_to_run,
                         input_file,
                         idx,
                         job_folder,
                         nb_nodes=1):
    script = f"""#!/bin/bash

#SBATCH -n {nb_nodes}
#SBATCH --time=48:00:00
#SBATCH --job-name={idx}
#SBATCH --mem-per-cpu=8192
python {file_to_run} --file {input_file}
"""
    job_script_name = job_folder + '/job-serial.sh'
    open(job_script_name, 'w').write(script)

    return job_script_name



path = '/cluster/work/cmbm/mpundir/pore-structure-study/output/'
schema = 'multiscale-higher-network-same'
runfile = '/cluster/work/cmbm/mpundir/pore-structure-study/simulations/multi_scale_2d.py'

runner = Runner()
runner.param['type']        = ['fractal', 'euclidean'] 
runner.param['rve']         = [1, 2]         
runner.param['flux']        = [1e-8]         # mol/mm2/s
runner.param['nb_steps']    = [2500]         # m
runner.param['time']        = [5000]         # sec


schema_folder = path + '/' + schema
if not os.path.exists(schema_folder):
    os.makedirs(schema_folder)

for param in runner.createParametricSpace():
    
    idx = uuid.uuid4().hex
    job_folder = schema_folder + '/' + idx
    if not os.path.exists(job_folder):
        os.makedirs(job_folder)

    arguments_file = ''
    for key, parameters in zip(runner.param.keys(), param):
        arguments_file += '--{} {} '.format(key, str(parameters))
    
    arguments_file += '--idx {} '.format(idx)
    arguments_file += '--path {} '.format(path)
    arguments_file += '--schema {} '.format(schema)

    arguments_filename = job_folder + '/arguments.dat'
    open(arguments_filename, 'w').write(arguments_file)
    
    job_script_name = create_sbatch_script(
        file_to_run=runfile,
        idx=idx,
        job_folder=job_folder,
        input_file=arguments_filename)
    
    result = subprocess.run(
        "sbatch --parsable {}".format(job_script_name),
        capture_output=True,
        text=True, shell=True).stdout.strip("\n")
   
    print('Job | ', result)
    
    schema_file = 'schema_space.h5'
    filename = path + '/'  + schema + '/' + schema_file
    
    with  h5py.File(filename, 'a') as h5file:
        grp = h5file.create_group(str(idx))
    
